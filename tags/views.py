from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

try:
    from tags.models import Tag
except Exception:
    Tag = None


# Create your views here.
# def show_tags(request):
#     context = {
#         "tags": Tag.objects.all() if Tag else None,
#     }
#     return render(request, "tags/list.html", context)
class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"


class TagDetailView(DetailView):
    model = Tag
    template_name = "tags/detail.html"

    def get_recipeset(self):
        

class TagCreateView(CreateView):
    model = Tag
    template_name = "tags/new.html"
    success_url = reverse_lazy("tags_list")
    fields = ["name", "recipes"]


class TagUpdateView(UpdateView):
    model = Tag
    template_name = "tags/edit.html"
    success_url = reverse_lazy("tags_list")
    fields = ["name", "recipes"]


class TagDeleteView(DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")
